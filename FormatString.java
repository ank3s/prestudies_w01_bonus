package anke.awacademy;

import java.util.Scanner;

public class FormatString {

    public static void formatString() {

        Scanner formatScanner = new Scanner(System.in);

        System.out.println("Bitte Text eingeben, der formatiert werden soll:");
        String input = formatScanner.nextLine();

        String[] upper = input.split("_");
        for (int i = 0; i < upper.length; i++) {
            if (i % 2 != 0) {
                upper[i] = upper[i].toUpperCase();
            }
        }

        String[] lower = String.join("", upper).split("#");
        for (int i = 0; i < lower.length; i++) {
            if (i % 2 != 0) {
                lower[i] = lower[i].toLowerCase();
            }
        }

        System.out.println(String.join("", lower));
    }
}